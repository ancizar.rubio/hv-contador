<?php

if(isset($_POST['submit']) && !empty($_POST['submit'])):
    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
        //your site secret key
        $secret = '6LflzRsUAAAAAPehlqNyhfaZRGFgdmWMyyvPRJnt';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success):
            //contact form submission code
            $name = !empty($_POST['nombres'])?$_POST['nombres']:'';
            $lastname = !empty($_POST['apellidos'])?$_POST['apellidos']:'';
            $tel = !empty($_POST['telefono'])?$_POST['telefono']:'';
            $email = !empty($_POST['email'])?$_POST['email']:'';
            
            $message = !empty($_POST['message'])?$_POST['message']:'';

            $services = $_POST["service"];

            for ($i=0;$i<count($services);$i++) {     
                echo "<br> service " . $i . ": " . $services[$i];    
            }
            
            $to = 'contacto@faroweb.com.co';
            $subject = 'Nuevo formulario de cotización enviado';
            $htmlContent = "
                <h1>Detalles de la solicitud de contacto</h1>
                <p><b>Nombres: </b>".$name."</p>
                <p><b>Apellidos: </b>".$lastname."</p>
                <p><b>Email: </b>".$email."</p>
                <p><b>Teléfono: </b>".$tel."</p>
                <p><b>Servicio: </b>".$services."</p>
                <p><b>Message: </b>".$message."</p>
            ";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers
            $headers .= 'From:'.$name.' <'.$email.'>' . "\r\n";
            //send email
            @mail($to,$subject,$htmlContent,$headers);
            
            $succMsg = 'Your contact request have submitted successfully.';
        else:
            $errMsg = 'Robot verification failed, please try again.';
        endif;
    else:
        $errMsg = 'Please click on the reCAPTCHA box.';
    endif;
else:
    $errMsg = '';
    $succMsg = 'Mensaje Enviado';
endif;

print $errMsg.$succMsg;
?>
