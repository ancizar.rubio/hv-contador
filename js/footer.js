var formS = document.querySelectorAll('select');
var instances = M.FormSelect.init(formS);

var btnShow = document.querySelectorAll('.show-more')[0];
var txtAbout = document.querySelectorAll('.about-us')[0];

btnShow.addEventListener("click",function(){
    if(!txtAbout.classList.contains('show-all')){
        txtAbout.classList.add('show-all');
        btnShow.innerHTML = "Leer menos";
        document.querySelectorAll('.profile-footer')[0].classList.add('hide');
    } else {
        txtAbout.classList.remove('show-all');
        btnShow.innerHTML = "Leer más";
        document.querySelectorAll('.profile-footer')[0].classList.remove('hide');
    }
})