document.addEventListener("DOMContentLoaded", function() {

  var sidenav = document.querySelectorAll('.sidenav');
  var instances = M.Sidenav.init(sidenav,{
    onOpenStart: function(){
      var txtmenu = document.querySelectorAll('.sidenav-trigger')[0].querySelectorAll('.material-icons')[0].innerHTML = 'close';
      side.setAttribute('style','z-index:99 !important')
    },
    onCloseStart: function(){
      var txtmenu = document.querySelectorAll('.sidenav-trigger')[0].querySelectorAll('.material-icons')[0].innerHTML = 'menu';
      side.removeAttribute('style')
    }
  });

  var fixedAbtn = document.querySelectorAll('.fixed-action-btn');
  var instances = M.FloatingActionButton.init(fixedAbtn);

  var toolTip = document.querySelectorAll('.tooltipped');
  var instances = M.Tooltip.init(toolTip);
  /*
  var elems = document.querySelectorAll(".scrollspy");
  var instances = M.ScrollSpy.init(elems,{
    scrollOffset: 65,
  });
  */

  // When the user scrolls down 50px from the top of the document, resize the header's font size
  window.onscroll = function() {
    scrollFunction();
  };

  var heightHeader = document.getElementsByClassName("banner")[0].offsetHeight;
  var side = document.querySelectorAll('.side')[0];

  function scrollFunction() {
    if (
      document.body.scrollTop > heightHeader ||
      document.documentElement.scrollTop > heightHeader - 80
    ) {
      document.getElementById("header").classList.add("sticky");
      side.classList.add('sticky-header');
    } else {
      document.getElementById("header").classList.remove("sticky");
      side.classList.remove('sticky-header');
    }
  }


  var btnContact = document.querySelectorAll('.phone-btn')[0];
  var btnEmail = document.querySelectorAll('.email-btn')[0];

  btnContact.addEventListener("click", function(e){
    e.preventDefault();    
    var phoneToast = '<span>316 523 8544</span><a class="btn-flat toast-action waves-effect" style="margin-left:0px !important><i class="material-icons white-text">phone</i></a>';
    M.toast({html: phoneToast, classes: 'rounded'});
  })

  btnEmail.addEventListener("click", function(e){
    e.preventDefault();
    var phoneToast = '<span>contacto@cesarlozanoram.com.co</span><a class="btn-flat toast-action waves-effect" style="margin-left:0px !important><i class="material-icons white-text">email</i></a>';
    M.toast({html: phoneToast, classes: 'rounded bottom'});
  })
  
  

});

var sections = $('.scrollspy');
 nav = $('nav');
 nav_height = nav.outerHeight();

$(window).on('scroll', function () {
  var cur_pos = $(this).scrollTop();
  
  sections.each(function() {
    var top = $(this).offset().top - 25 - nav_height,
        bottom = top + $(this).outerHeight();
    
    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('a.scroll').removeClass('active');
      sections.removeClass('active');
      
      $(this).addClass('active');
      nav.find('a.scroll[href="#'+$(this).attr('id')+'"]').addClass('active');
    }
  });
});

$('a.scroll').on('click', function () {
  var $el = $(this);
  id = $el.attr('href');
  
  $('html').animate({
    scrollTop: $(id).offset().top - nav_height
  }, 500);
  
  return false;
});