 

function initMap() {

    var myLatLng = {lat: 4.1496768, lng: -74.8872382}

    var map = new google.maps.Map(document.getElementById('map'), {

        center: myLatLng,

        zoom: 16,                

    });

    var marker = new google.maps.Marker({

        position: myLatLng,

        map: map,

        title: 'Cesar Enrique Lozano Ramírez, Contador publico'

    });

}





/*** FORMULARIO DE CONTACTO ***/


var formContact = document.getElementById("contactForm");

var formData = new FormData();

var fileField = formContact.querySelectorAll("input");

var btnSend = formContact.querySelector("button");



function validateForm() {

  var state = true;

  for (var i = 0; i < fileField.length; i++) {

    if (fileField[i].value == "" && fileField[i].required == true) {

      state = false

    }

  }

  return state

}

 


function infoSendForm(e) {

  event.preventDefault();

  var responseContact = grecaptcha.getResponse();

  console.log(responseContact);



  var formData = new FormData();

  //formData.append('nombres', String("sebas"));

  formData.append('nombres', String(document.getElementById("nombres").value));

  formData.append('apellidos', String(document.getElementById("apellidos").value))

  formData.append('email', String(document.getElementById("email").value))

  formData.append('tel', String(document.getElementById("tel").value))

  //formData.append('tel ', String("213123"));

  formData.append('services', String(document.getElementById("services").value))

  formData.append('mensaje', String(document.getElementById("mensaje").value))

  formData.append("g-recaptcha-response", String(responseContact));



  var stateform = validateForm()

  if (stateform && responseContact != "") {

    fetch("php/phpmailsend.php", {

      method: "POST",

      body: formData,

      contentType: false,

    })

      .then(function (response) {

        document.querySelectorAll('.progress')[0].classList.remove('hide');
        btnSend.setAttribute('disabled');

        if (response.status == 200) {

          M.toast({ html: 'información enviada, pronto estaremos en contácto.', classes: 'rounded green' });

          document.getElementById("nombres").value = "";

          document.getElementById("apellidos").value = "";

          document.getElementById("email").value = "";

          document.getElementById("tel").value =""

          document.getElementById("mensaje").value =""

          document.querySelectorAll('.progress')[0].classList.add('hide');

          btnSend.removeAttribute('disabled');

          grecaptcha.reset();

        }else{

          M.toast({ html: 'Por favor intente más tarde', classes: 'rounded red' });

          document.querySelectorAll('.progress')[0].classList.add('hide');
          btnSend.removeAttribute('disabled');

        }

      })

      .then(function (myJson) {

      }).catch((error) => {

        M.toast({ html: 'Por favor intente más tarde', classes: 'rounded red' });

        document.querySelectorAll('.progress')[0].classList.add('hide');
        btnSend.removeAttribute('disabled');

      });

  } else {

    M.toast({ html: 'Completa el captcha!', classes: 'rounded red' });

    M.toast({ html: 'existen campos vacíos', classes: 'rounded red' });

  }



}
